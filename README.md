# picter/gitlab-label-bot

Bot to adds labels to automate your gitlab workflow.

## Setup

1. Make a copy of the `.env.example` and name it `.env`.
2. Add Gitlab `Private Token` to `.env`. See https://docs.gitlab.com/ce/api/README.html#private-tokens
3. Install dependencies via `yarn` or `npm install`.
4. Start service via `npm start` (nodemon). Feel free to use something else, just compile `TypeScript` first and use `src/index.ts` as entry point.
5. Configure `Web Hook` on Gitlab to push events to `<your-ip>:11180`. See https://docs.gitlab.com/ce/user/project/integrations/webhooks.html

Configuring `sentry` is optional. See https://docs.sentry.io/quickstart/. Add the `DSN` to the `.env` file if you wish to enable error reporting.
