import { arrayEqualContent } from './array-equal-elements'

it('The same array is equal.', () => {
  expect(arrayEqualContent([1, 2, 3], [1, 2, 3])).toBeTruthy()
})

it('Different arrays are not equal.', () => {
  expect(arrayEqualContent([1, 2, 3], [1, 2])).toBeFalsy()
})

it('Arrays with same content but different order are equal.', () => {
  expect(arrayEqualContent([1, 3, 2], [1, 2, 3])).toBeTruthy()
})
