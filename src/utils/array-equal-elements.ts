import { isEqual } from 'lodash'

export const arrayEqualContent = (array1: Array<any>, array2: Array<any>) =>
  isEqual(array1.sort(), array2.sort())
