import axios from 'axios'

import { transformProjectName } from '../transforms/project-name'

const HOST = 'https://gitlab.com'

axios.defaults.headers['PRIVATE-TOKEN'] = process.env.GITLAB_ACCESS_TOKEN

export const getIssue = async (projectName, issueIid) => {
  const response = await axios.get(
    `${HOST}/api/v3/projects/${transformProjectName(
      projectName,
    )}/issues?iid=${issueIid}`,
  )
  return response.data[0]
}

export const updateIssue = async (
  projectName,
  issueId,
  labels: Array<string>,
) => {
  const projectId = transformProjectName(projectName)
  const labelsString = labels.join(',')
  const response = await axios.put(
    `${HOST}/api/v3/projects/${projectId}/issues/${issueId}?labels=${labelsString}`,
  )
  return response.data[0]
}
