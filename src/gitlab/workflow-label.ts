export enum WorkflowLabel {
  ToDo = <any>'To Do',
  Doing = <any>'Doing',
  MergeRequest = <any>'Merge Request',
}
