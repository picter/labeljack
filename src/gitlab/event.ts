// https://docs.gitlab.com/ce/user/project/integrations/webhooks.html#events
type GitlabObjectKind =
  | 'push'
  | 'tag_push'
  | 'issue'
  | 'note'
  | 'merge_request'
  | 'wiki_page'
  | 'pipeline'
  | 'build'

export interface GitlabEvent {
  object_kind: GitlabObjectKind
  project?: any
  repository?: any
  object_attributes?: any
  ref?: any
}
