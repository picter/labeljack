import { GitlabEvent } from '../gitlab/event'

import { WorkflowLabel } from '../gitlab/workflow-label'
import { getIssue, updateIssue } from '../requests/issue'
import { transformBranchRef } from '../transforms/branch-ref'
import { arrayEqualContent } from '../utils/array-equal-elements'

export const updateLabels = (labels: Array<any>) => {
  const cleanedLabels = labels.filter(label => label !== WorkflowLabel.ToDo)
  return cleanedLabels.includes(WorkflowLabel.MergeRequest) ?
    cleanedLabels :
    [WorkflowLabel.Doing, ...cleanedLabels]
}

const handler = (config?) =>
  async (event: GitlabEvent) => {
    const branchIssueIid = transformBranchRef(event.ref)
    if (!branchIssueIid) {
      return { msg: 'Invalid branch ref', value: event.ref }
    }

    const issue = await getIssue(event.project.path_with_namespace, branchIssueIid)
    const nextLabels = updateLabels(issue.labels)

    if (arrayEqualContent(issue.labels, nextLabels)) {
      return { msg: 'Labels did not change.', value: nextLabels }
    }

    return await updateIssue(event.project.path_with_namespace, issue.id, nextLabels)
  }

export default handler
