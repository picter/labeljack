import { GitlabEvent } from '../gitlab/event'
import doing, { updateLabels } from './label-doing'

it('Has handle method implemented.', () => {
  const event: GitlabEvent = { object_kind: 'push', ref: 'refs/heads/master' }
  expect(doing()(event)).toBeTruthy()
})

it('Invalid branch ref gets rejected.', async () => {
  const event: GitlabEvent = { object_kind: 'push', ref: 'refs/heads/master' }
  expect(await doing()(event)).toEqual({
    msg: 'Invalid branch ref',
    value: event.ref,
  })
})

it('Add doing label to empty list of labels.', () => {
  const result = updateLabels([])
  const expected = ['Doing']
  expect(result).toEqual(expected)
})

it('Other random labels are not affected.', () => {
  const previous = ['a', 'label', 'test']
  const result = updateLabels(previous)
  const expected = ['Doing', ...previous]
  expect(result).toEqual(expected)
})

it('Labels of previous workflow step are removed.', () => {
  const result = updateLabels(['a', 'To Do', 'label', 'test'])
  const expected = ['Doing', 'a', 'label', 'test']
  expect(result).toEqual(expected)
})

it('Do not apply Doing label if a Merge request exists.', () => {
  const result = updateLabels(['a', 'To Do', 'label', 'test', 'Merge Request'])
  const expected = ['a', 'label', 'test', 'Merge Request']
  expect(result).toEqual(expected)
})
