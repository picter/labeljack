import { GitlabEvent } from '../gitlab/event'

import { WorkflowLabel } from '../gitlab/workflow-label'
import { getIssue, updateIssue } from '../requests/issue'
import { transformBranchToIssue } from '../transforms/branch'

export const updateLabels = labels =>
  [WorkflowLabel.MergeRequest, ...labels.filter(label => ['Doing', 'To Do'].indexOf(label) === -1)]

const handler = (config?) =>
  async (event: GitlabEvent) => {
    const branchIssueIid = transformBranchToIssue(event.object_attributes.source_branch)
    if (!branchIssueIid) {
      return { msg: 'Invalid branch name', value: event.object_attributes.source_branch }
    }
    const issue = await getIssue(event.project.path_with_namespace, branchIssueIid)

    const nextLabels = updateLabels(issue.labels)

    return await updateIssue(event.project.path_with_namespace, issue.id, nextLabels)
  }

export default handler
