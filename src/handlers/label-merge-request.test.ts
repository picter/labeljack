import { GitlabEvent } from '../gitlab/event'
import mergeRequest, { updateLabels } from './label-merge-request'

it('Has handle method implemented.', () => {
  const event: GitlabEvent = { object_kind: 'merge_request', object_attributes: { source_branch: 'test' } }
  expect(mergeRequest()(event)).toBeTruthy()
})

it('Empty list of labels gets merge request added.', () => {
  const result = updateLabels([])
  const expected = ['Merge Request']
  expect(result).toEqual(expected)
})

it('Other labels are not affected.', () => {
  const previous = ['a', 'label', 'test']
  const result = updateLabels(previous)
  const expected = ['Merge Request', ...previous]
  expect(result).toEqual(expected)
})

it('Labels of other workflow steps should be removed.', () => {
  const result = updateLabels(['a', 'Doing', 'label', 'test'])
  const expected = ['Merge Request', 'a', 'label', 'test']
  expect(result).toEqual(expected)
})
