import { GitlabEvent } from '../gitlab/event'

export const objectTypeGuard = (objectType: string, fn: Function) => async (
  event: GitlabEvent,
) => (event.object_kind === objectType ? await fn(event) : null)
