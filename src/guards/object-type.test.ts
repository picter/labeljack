import { objectTypeGuard } from './object-type'
import { GitlabEvent } from '../gitlab/event'

it('Skip function if object type does not match.', () => {
  const mockCallback = jest.fn()
  const guard = objectTypeGuard('merge_request', mockCallback)
  const payload: GitlabEvent = { object_kind: 'push' }
  guard(payload)
  expect(mockCallback).not.toBeCalled()
})

it('Match object type in request.', () => {
  const mockCallback = jest.fn()
  const guard = objectTypeGuard('merge_request', mockCallback)
  const payload: GitlabEvent = { object_kind: 'merge_request' }
  guard(payload)
  expect(mockCallback).toBeCalled()
})
