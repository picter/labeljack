import { transformBranchRef } from './branch-ref'

it('Transform branch ref to issue id.', () => {
  expect(transformBranchRef('refs/heads/1-test')).toEqual(1)
})

it('Invalid branch ref returns falsy value.', () => {
  expect(transformBranchRef('refs/heads/master')).toBeFalsy()
})
