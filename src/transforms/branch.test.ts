import { transformBranchToIssue } from './branch'

it('Convert branch name to issue id.', () => {
  expect(transformBranchToIssue('7-add-test-setup')).toEqual(7)
})

it('Branch without dashes can is falsy.', () => {
  expect(transformBranchToIssue('somefancybranch')).toBeFalsy()
})

it('Branch without issue number is falsy.', () => {
  expect(transformBranchToIssue('add-test-setup')).toBeFalsy()
})
