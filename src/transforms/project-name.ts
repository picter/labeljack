export const transformProjectName = (projectName: string) => projectName.replace(/\//, '%2F')
