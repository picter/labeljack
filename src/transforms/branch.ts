export const transformBranchToIssue = (branchName: string) =>
  +branchName.split('-')[0]
