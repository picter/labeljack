import { transformProjectName } from './project-name'

it('Transform project name to gitlab url part.', () => {
  expect(transformProjectName('picter/gitlab-label-bot')).toEqual('picter%2Fgitlab-label-bot')
})
