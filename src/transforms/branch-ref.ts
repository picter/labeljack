import { transformBranchToIssue } from './branch'

export const transformBranchRef = (branchRef: string) => {
  const parts = branchRef.split('/')
  const branchName = parts[parts.length - 1]
  return transformBranchToIssue(branchName)
}
