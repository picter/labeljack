const typescript = require('typescript')

const config = require('./tsconfig.json')

module.exports = {
  process(src, path) {
    if (path.endsWith('.ts')) {
      return typescript.transpile(src, config.compilerOptions, path, [])
    }
    return src
  },
}
