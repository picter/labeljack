const path = require('path')

module.exports = {
  entry: path.resolve(__dirname, 'handler.ts'),
  resolve: {
    extensions: ['.ts', '.js', '.json'],
  },
  target: 'node',
  output: {
    libraryTarget: 'commonjs',
    path: path.resolve(__dirname, '.webpack'),
    filename: 'handler.js',
  },
  module: {
    rules: [{ test: /\.ts$/, use: 'ts-loader' }],
  },
}
