import { Client as Raven } from 'raven'

import labelMergeRequest from './src/handlers/label-merge-request'
import labelDoing from './src/handlers/label-doing'
import { objectTypeGuard } from './src/guards/object-type'

export const main = async (event, context, callback) => {
  const raven = new Raven(process.env.SENTRY_DSN_URL).install()

  process.on('unhandledRejection', error => {
    raven.captureException(error)
  })

  const { body } = event

  let payload
  try {
    payload = JSON.parse(body)
  } catch (error) {
    return callback(null, {
      statusCode: 400,
      message: error.message,
    })
  }

  const results = [
    await objectTypeGuard('merge_request', labelMergeRequest())(payload),
    await objectTypeGuard('push', labelDoing())(payload),
  ]

  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
      input: event,
      payload,
      results,
    }),
  }

  return callback(null, response)
}
