import axios from 'axios'
import * as MockAdapter from 'axios-mock-adapter'

import { main } from './handler'

const gitlabPush = JSON.stringify(require('./mocks/gitlab-push.json'))
const gitlabIssue35 = require('./mocks/gitlab-issue-35.json')

const mock = new MockAdapter(axios)

mock
  .onGet('https://gitlab.com/api/v3/projects/picter%2Flabeljack/issues?iid=35')
  .reply(200, gitlabIssue35)
mock
  .onPut(
    'https://gitlab.com/api/v3/projects/picter%2Flabeljack/issues/6678288?labels=Doing',
  )
  .reply(200, ['A61B77C6-C42C-483E-921C-801009B3ED1A'])
mock.onAny().reply((...args) => {
  console.log(args)
  return [500, {}]
})

it('Main handler is defined.', () => {
  expect(main).toBeTruthy()
})

it('Main handler refuses non JSON payloads.', done => {
  const callback = (err, res) => {
    expect(err).toBeNull()
    expect(res.statusCode).toBe(400)
    done()
  }

  main({ body: '' }, {}, callback)
})

it('Main handler accepts JSON payloads.', done => {
  const callback = (err, res) => {
    expect(err).toBeNull()
    expect(res.statusCode).toBe(200)
    done()
  }

  main({ body: '{}' }, {}, callback)
})

it('Handle push event.', async () => {
  const mockCallback = jest.fn()
  await main({ body: gitlabPush }, {}, mockCallback)
  expect(mockCallback).toBeCalled()
  expect(mockCallback.mock.calls.length).toBe(1)

  // error (first param) should be null
  expect(mockCallback.mock.calls[0][0]).toBeNull()

  // res (second param)
  const res = mockCallback.mock.calls[0][1]
  expect(res).toEqual(
    expect.objectContaining({
      body: expect.any(String),
      statusCode: 200,
    }),
  )

  const payload = JSON.parse(res.body)
  expect(payload).toBeTruthy()
  expect(payload.results).toEqual([
    null,
    'A61B77C6-C42C-483E-921C-801009B3ED1A',
  ])
})
